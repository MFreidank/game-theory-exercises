#include "Player.h"
#include <Eigen/Dense>
#include "../lp_solve_5.5.2.0_dev_ux64/lp_lib.h"
#ifndef GAME_H
#define GAME_H 

using UtilityMatrix = std::vector<std::vector<std::tuple<double, double>>>;
using pair_utility= std::tuple<double, double>;
using Profile = std::tuple<size_t, size_t>;
using Support = std::tuple<std::vector<std::string>, std::vector<std::string>>;
using Powerset = std::vector<std::vector<std::string>>;

class Game
{
public:

    Game(Player p1, Player p2, UtilityMatrix &utilities);

    inline std::tuple<double, double> utility(size_t actionIndexP1, size_t actionIndexP2) const
    {
        return utilities_[actionIndexP1-1][actionIndexP2-1];
    }

    double payoff (size_t playerIndex, Profile profile) const;


    std::vector<Support> enumerateAllSupports(std::vector<std::string> actionsP1, std::vector<std::string> actionsP2);

    void generateAndSolveLP(Support supp);

    
    void printActions() const;
    void printUtilities() const;

private:

    Powerset generatePowerSet(std::vector<std::string> elts);

    Player* player1_;
    Player* player2_;
    UtilityMatrix utilities_;
};
#endif
