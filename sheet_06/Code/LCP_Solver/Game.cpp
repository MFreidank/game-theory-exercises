#include "Game.h"
#include <assert.h>
// TODO: Replace "Support" by "support tuple" in using directive.
// TODO: Add more using directives to make code more readable

Game::Game(Player p1, Player p2, UtilityMatrix &utilities)
{
    player1_ = &p1;
    player2_ = &p2;
    utilities_ = utilities;
}

double Game::payoff(size_t playerIndex, Profile profile) const
{
    assert(playerIndex == 1 || playerIndex == 2);
    if(playerIndex == 1)
    {
        return std::get<0>((utility(std::get<0>(profile),std::get<1>(profile))));
    } else 
    {
        return std::get<1>((utility(std::get<0>(profile),std::get<1>(profile))));
    }
}

void Game::generateAndSolveLP(Support supp)
{
  // Initialize support sets for the current lp.
  std::vector<std::string> supp_alpha = std::get<0>(supp);
  std::vector<std::string> supp_beta = std::get<1>(supp);

  lprec *lp;
  int Ncol, *colno = NULL, j, ret = 0;
  REAL *row = NULL;

  Ncol = 12; /* we have 12 variables (alpha(p1),...,alpha(p5), beta(p1),...,beta(p5), u, v*/
  lp = make_lp(0, Ncol);

  if(lp == NULL)
  {
    ret = 1; /* could not construct a new model */
  }


  if(ret == 0)
  {
    set_col_name(lp, 1, "alpha(p1)");
    set_col_name(lp, 2, "alpha(p2)");
    set_col_name(lp, 3, "alpha(p3)");
    set_col_name(lp, 4, "alpha(p4)");
    set_col_name(lp, 5, "alpha(p5)");
    set_col_name(lp, 6, "beta(p1)");
    set_col_name(lp, 7, "beta(p2)");
    set_col_name(lp, 8, "beta(p3)");
    set_col_name(lp, 9, "beta(p4)");
    set_col_name(lp, 10, "beta(p5)");
    set_col_name(lp, 11, "u");
    set_col_name(lp, 12, "v");


    /* create space large enough for one row */
    colno = (int *) malloc(Ncol * sizeof(*colno));
    row = (REAL *) malloc(Ncol * sizeof(*row));
    if((colno == NULL) || (row == NULL))
    {
      ret = 2;
    }
  }
  if(ret == 0)
  {
    set_add_rowmode(lp, TRUE);

    /* construct constraints of the form: u-U_1(a_i, beta) */

    for (size_t p1 = 1; p1 < 6; ++p1)
    {
      j = 0;
      int i = 1;
      for (; i < 7; ++i)
      {
        colno[j] = i;
        row[j++] = 0;
      }

      for (size_t p2 = 1; p2 < 6; ++p2)
      {
        ++i;
        colno[j] = i;
        double val = -1 * payoff(1, std::tuple<size_t, size_t>(p1,p2));
        row[j++] = val;
      }

        colno[j] = 11;
        row[j++] = 1;
        colno[j] = 12;
        row[j++] = 0;

        for (int p = 0; p < 12; ++p)
        {
          std::cout << colno[p] << ": " << row[p+1] << std::endl;
        }
        if(!add_constraintex(lp, j, row, colno, GE, 0))
        {
          ret = 3;
        }
      }
    }
}


std::vector<Support> Game::enumerateAllSupports(std::vector<std::string> actionsP1, std::vector<std::string> actionsP2)
{
    auto powerSetP1 = generatePowerSet(actionsP1);
    auto powerSetP2 = generatePowerSet(actionsP2);

    std::vector<Support> allSupports;

    for (auto &set1 : powerSetP1)
    {
        if(set1.size() > 0)
        {
            for (auto &set2 : powerSetP2)
            {
                if(set2.size() > 0)
                {
                    allSupports.push_back(Support(set1, set2));
                }
            }
        }
    }
    return allSupports;
}

Powerset Game::generatePowerSet(std::vector<std::string> elts)
{
    if(elts.empty())
    {
        return Powerset(1, std::vector<std::string>());
    } else 
    {

        Powerset allofthem = generatePowerSet(std::vector<std::string>(elts.begin()+1, elts.end()));
        std::string elt = elts[0];
        const size_t n = allofthem.size();
        for (size_t i = 0; i < n; ++i)
        {
            const std::vector<std::string> &s = allofthem[i];
            allofthem.push_back(s);
            allofthem.back().push_back(elt);
        }
        return allofthem;
    }
}




void Game::printActions() const
{
    player1_->printActions();
    player2_->printActions();
}

void Game::printUtilities() const
{
    for (size_t i = 0; i < 5; ++i)
    {
        for (size_t j = 0; j < 5; ++j)
        {
            std::cout << std::get<0>((utilities_[i])[j]) << " ";
            std::cout << std::get<1>((utilities_[i])[j]) << std::endl;
        }
    }
}
