#include "Player.h"
using namespace std;

Player::Player(size_t id, vector<string> actions)
{
    actions_ = actions;
    id_ = id;
}

void Player::printActions() const
{
    for (auto &a : actions_)
    {
        cout << a << endl;
    }
}
