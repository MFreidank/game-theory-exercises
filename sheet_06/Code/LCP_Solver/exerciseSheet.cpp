#include "Game.h"
using namespace std;

int main(int argc, char *argv[])
{
    vector<string> actions {"p1", "p2", "p3", "p4", "p5"};
    UtilityMatrix utilities(5, std::vector<pair_utility>(5));
    for (size_t i = 0; i < 5; ++i)
    {
        for (size_t j = 0; j < 5; ++j)
        {
            double payoffP1 = (i==j)? 0:i+1;
            double payoffP2 = (i==j)? 0:j+1;
            (utilities[i])[j] = std::tuple<double, double>(payoffP1, payoffP2);
        }
    }
    Player p1(1, actions); 
    Player p2(2, actions); 
    Game g1(p1,p2, utilities);
    std::vector<Support> supports = g1.enumerateAllSupports(actions, actions);
/*    for (auto &v : supports)
    {
        auto v1 = std::get<0>(v);
        auto v2 = std::get<1>(v);
        std::cout << "[ {"; 
        for (auto &s : v1)
        {
            std::cout << s << " ";
        }

        std::cout << "} {"; 
        for (auto &t : v2)
        {
            std::cout << t << " ";
        }
        std::cout << "} ]"<<std::endl; 
    }*/
    std::cout << supports.size() << std::endl;
    g1.generateAndSolveLP(supports[0]);
    return 0;
}
