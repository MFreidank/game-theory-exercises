#include <vector>
#include <string>
#include <iostream>
#ifndef PLAYER_H
#define PLAYER_H 
class Player
{
public:
    Player (size_t id, std::vector<std::string> actions);

    inline size_t getId() const
    {
        return id_;
    }

    void printActions() const;

    inline std::vector<std::string> getActions() const
    {
      return actions_;
    }



private:
    std::vector<std::string> actions_;
    size_t id_;
};

#endif
