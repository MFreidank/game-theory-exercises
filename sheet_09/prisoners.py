#!/usr/bin/env python

class GameMaster(object):
    def __init__(self, horizon):
        self.horizon   = horizon
        self.player1   = None
        self.player2   = None
        self.lastmove1 = None
        self.lastmove2 = None
        self.score1    = 0
        self.score2    = 0
        self.history = list()

    def register(self, player):
        assert not self.player1 or not self.player2
        #assert self.round == 0
        if not self.player1:
            self.player1 = player
        elif not self.player2:
            self.player2 = player

    def update(self,move1,move2):
        move1 = move1.upper()
        move2 = move2.upper()
        self.history.append((move1, move2))
        self.lastmove1 = move1
        self.lastmove2 = move2
        if move1 == "C":
            if move2 == "C":
                self.score1 = self.score1 + 1
                self.score2 = self.score2 + 1
            elif move2 == "D":
                self.score1 = self.score1 + 4
                self.score2 = self.score2 + 0
            else:
                assert False
        elif move1 == "D":
            if move2 == "C":
                self.score1 = self.score1 + 0
                self.score2 = self.score2 + 4
            elif move2 == "D":
                self.score1 = self.score1 + 3
                self.score2 = self.score2 + 3
            else:
                assert False
        else:
            assert False

    def playRound(self):
        assert self.player1 and self.player2
        move1 = self.player1.nextmove(self.lastmove2)
        move2 = self.player2.nextmove(self.lastmove1)
        self.update(move1,move2)

    def play(self):
        for round in range(self.horizon):
            self.playRound()
        print "Score of player 1: %d" % self.score1
        print "Score of player 2: %d" % self.score2
        


class Player():
    def __init__(self, horizon):
        self.horizon = horizon # number of times the game is played repeatedly
        self.history = []      # contains tuples representing players past moves
        self.previousmove = None

    def nextmove(self, opponentmove):
        """ returns the players next move, given the opponents previous move

        Arguments:
            opponentmove - string representing the opponents previous move, its
            value is either "C" (confess) or "D" (dont confess)

        Returns:
            either "C" or "D"

        """
        # TODO: implement your strategy
        pass


class TitForTatPlayer(Player):
    """ This player implements a tit-for-tat strategy

        Example:
        >>> player = Player(35)
        >>> move = player.nextmove("C")
        >>> move
        'C'
        >>> move = player.nextmove("D")
        >>> move
        'D'
    """
    def nextmove(self, opponentmove):
        self.history.append((self.previousmove, opponentmove))
        if not opponentmove:
            self.previousmove = "D"
            return "D"
        if opponentmove == "C":
            self.previousmove = "C"
            return "C"
        else:
            self.previousmove = "D"
            return "D"


class OurPlayer(Player):
    def nextmove(self, opponentmove):
      self.history.append((self.previousmove, opponentmove))
      self.previousmove = "C"
      return "C"


if __name__ == "__main__":
    horizon = 100
    gm = GameMaster(horizon)
    p1 = OurPlayer(horizon)
    p2 = TitForTatPlayer(horizon)
    gm.register(p1)
    gm.register(p2)
    gm.play()
    print gm.history