#include "randomGameCreator.h"
/*
 * Random Game Creator. 
 * This is a small application to 
 * generate random game-matrices for 2-Player Games of 
 * any given size.
 * Allows requesting zero-sum games explicitly.
*/



randomGameCreator::randomGameCreator(size_t no_actions_P1, size_t no_actions_P2, bool is_zsg)
{
    assert(no_actions_P1 > 0 && no_actions_P2 > 0);
    dimX_ = no_actions_P1;
    dimY_ = no_actions_P2;
    zsg_ = is_zsg;

    drawMatrix();
}

void randomGameCreator::drawMatrix()
{
    GameMatrix values = getValues();

    for (size_t k = 0; k != dimY_; k++) 
    {
        if(k == 0)
        {
            std::cout << " _________";
        } else 
        {
            std::cout << "_________";
        }
    }
    std::cout << "\n";
    for (size_t i = 0; i < values.size(); ++i)
    {
        auto curr = values.at(i);
        for (size_t j = 0; j < curr.size() ; ++j)
        {
            std::cout << " | ";
            auto pair = curr.at(j);
            std::cout << "(" << get<0>(pair) << ", " << get<1>(pair) << ")";
        }
        std::cout << " | ";
        std::cout << "\n";
        std::cout << " " << std::endl;
        for (size_t k = 0; k != dimY_; k++) 
        {
            if(k == 0)
            {
                std::cout << " _________";
            } else 
            {
                std::cout << "_________";
            }
        }
        std::cout << "\n";
    }
    for (size_t i = 0; i != values.size(); i++) 
    {
        auto curr = values.at(i);
        for (size_t j = 0; j != curr.size(); j++) 
        {
            auto pair = curr.at(j);
            std::cout << get<0>(pair) << " ";
        }
        std::cout << "\n";
    }
    std::cout << std::endl;

    for (size_t i = 0; i != values.size(); i++) 
    {
        auto curr = values.at(i);
        for (size_t j = 0; j != curr.size(); j++) 
        {
            auto pair = curr.at(j);
            std::cout << get<1>(pair) << " ";
        }
        std::cout << "\n";
        
        
    }
    
}

GameMatrix randomGameCreator::getValues()
{
    std::vector<vector<tuple<int, int>> > rows;
    srand(time(NULL));
    for (size_t i = 0; i < dimX_; i++) 
    {
        vector<tuple<int, int> > column;
        for (size_t j = 0; j < dimY_; j++) 
        {
            int a = rand()%20;
            int b;
            if(zsg_)
            {
                b = -1*a;
            } else 
            {
                b = rand()%20;
            }
            
            auto val = make_tuple(a,b);
            column.push_back(val);
        }
        rows.push_back(column);
    }
    return rows;
}



bool handleBooleanUserInput()
{
    string in;
    cin >> in;
    bool input;
    istringstream(in.c_str()) >> std::boolalpha >> input;
    if(!input)
    {
        istringstream(in.c_str()) >> input;
    }
    return input;
}

int main(int argc, char *argv[])
{
    std::cout << "Enter number of actions of P1:" << std::endl;
    size_t actions_p1;
    cin >> actions_p1;
    std::cout << "Enter number of actions of P2:" << std::endl;
    size_t actions_p2;
    cin >> actions_p2;
    std::cout << "Should the game be a zsg?" << std::endl;
    // Handle boolean input "true/false" as well as "1/0"
    bool zsg = handleBooleanUserInput();

    randomGameCreator rg(actions_p1, actions_p2, zsg);
    
    return 0;
}

