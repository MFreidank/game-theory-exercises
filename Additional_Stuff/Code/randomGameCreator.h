#ifndef RANDOM_GAME_CREATOR_H
#define RANDOM_GAME_CREATOR_H 
#include <cstddef>
#include <vector>
#include <tuple>
#include <iostream>
#include <algorithm>
#include <functional>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string>
#include <sstream>
using namespace std;
typedef vector<vector<tuple<int, int> >> GameMatrix;
class randomGameCreator
{
public:
    randomGameCreator(size_t no_actions_P1, size_t no_actions_P2, bool is_zsg);

private:
    
    GameMatrix getValues();
    void drawMatrix();


    size_t dimX_;
    size_t dimY_;
    bool zsg_;
};
#endif
